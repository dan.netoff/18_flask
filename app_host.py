# Hello
from config import Configuration
from datetime import datetime
import models
from flask import Flask, render_template, send_from_directory, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object(Configuration)
app.config['UPLOAD_FOLDER'] = './static/music/'

db = SQLAlchemy(app)


@app.route('/')
def hello_world():
    q = request.args.get('q')
    if q:
        songs = models.Song.query.filter(models.Song.song.contains(q) | models.Song.artist.contains(q) | models.Song.album.contains(q)).all()
    else:
        songs = models.Song.query.all()
    return render_template('index_page.html', songs=songs)


@app.route('/artists')
def show_artists():
    artists = db.session.query(models.Song, db.func.count(models.Song.id)).group_by(models.Song.artist).all()
    return render_template('artists.html', artists=artists)


@app.route('/albums')
def show_albums():
    albums = db.session.query(models.Song, db.func.count(models.Song.id)).group_by(models.Song.album).all()
    return render_template('albums.html', albums=albums)


@app.route('/<path:filename>')
def download_file(filename):
    context = {
        'my_file': send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True),
        'my_file_name': filename
    }
    return render_template('albums2.html', **context)

