from app import db
from datetime import datetime


class Song(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    song = db.Column(db.String(140), unique=True)
    artist = db.Column(db.String(140))
    album = db.Column(db.String(140))
    duration = db.Column(db.String(140))
    uploader = db.Column(db.String(140))
    upload_date = db.Column(db.DateTime, default=datetime.now())


