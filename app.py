# config.py = секьюрный файл, там конекты к БД и т.д.. его лучше .gitignore
# my last commit
from config import Configuration
import models
from flask import Flask, render_template, request, send_from_directory
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_object(Configuration)
app.config['UPLOAD_FOLDER'] = './static/music/'
db = SQLAlchemy(app)

# 2. Не доделана миграция((
from flask_migrate import Migrate, MigrateCommand
# migrate = Migrate(app, db)
# manager = Manager(app)
# manager.add_command('db', MigrateCommand)


@app.route('/')
def hello_world():
    # если есть параметр - знач искали, выведем результаты поиска
    q = request.args.get('q')
    if q:
        songs = models.Song.query.filter(models.Song.song.contains(q) | models.Song.artist.contains(q) | models.Song.album.contains(q)).all()
    else:
        songs = models.Song.query.all()
    return render_template('index_page.html', songs=songs)


@app.route('/artists')
def show_artists():
    # по старому в sqlite3:
    # conn = create_connection('myDB.sqlite3')
    # cursor = conn.cursor()
    # cursor.execute('SELECT count(id_songs) as myCount, artist FROM songs GROUP BY artist')
    # artists = cursor.fetchall()
    # sqlAlchemy + group_by():
    artists = db.session.query(models.Song, db.func.count(models.Song.id)).group_by(models.Song.artist).all()
    context = {
        'artists': artists
    }
    return render_template('artists.html', **context)


@app.route('/albums')
def show_albums():
    albums = db.session.query(models.Song, db.func.count(models.Song.id)).group_by(models.Song.album).all()
    return render_template('albums.html', albums=albums)


# send_from_directory - безопасная? работа с файлами
@app.route('/music/<path:filename>')
def download_file(filename):
    context = {
        'my_file': send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True),
        'my_file_name': filename
    }
    return render_template('albums2.html', **context)


if __name__ == '__main__':
    app.run()

